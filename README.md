# Stimulus Remote

Stimulus controller to provide content handling for HTML sent over the wire whilst using Rails UJS

## Installation

```shell
$ yarn add stimulus-remote
```

## Usage

Register the controller with Stimulus:

```javascript
// application.js
import { Application } from 'stimulus';
import { RemoteController } from 'stimulus-remote'

const application = Application.start()
application.register('remote', RemoteController)
```

Initialize the controller on a container element, and add an action to render the response:

```erb
<%= form_with model: @model, data: { controller: 'remote', action: 'ajax:error->remote#replace' } do |f| %>
  <%= f.text_field :field_name %>
  <%= f.submit %>
<% end %>
```

In the Rails controller, you will need to modify your response:

```ruby
def create
  @model = Model.new model_params
  if @model.save
    redirect_to models_path, notice: 'Success!'
  else
    render partial: 'form', status: :unprocessable_entity
  end
end
```

You can see that the form partial has been separated so it is the only thing
returned in the response. The response also sets an Unprocessable Entity HTTP
Status Code (422). This is important so that Rails UJS emits a 'ajax:error'
event.

### Options

#### `data-remote-load-scripts` (default: `false`)
Setting this attribute on the controller element will eval() scripts in the response if set. *This is intentionally off by default.*

##### Example

```erb
<div data-controller="remote" data-remote-load-scripts>
  <%= link_to 'Click me', foo_path, data: { action: 'ajax:success->remote#replace' } %>
</div>
```

#### `data-remote-response-target` (default: `this.element`)
Use this attribute to change the element that will have its contents changed. This can be any CSS selector and does not have to be within the scope of the controller.

##### Example

```erb
<div id="response-target"></div>
<div data-controller="remote" data-remote-response-target="#response-target">
  <%= link_to 'Click me', foo_path, data: { action: 'ajax:success->remote#replace' } %>
</div>
```

#### `data-remote-debounce-time` (default: `300`)
Change the debounce time for the `debounceSubmit` method in ms.

### Methods

#### `submit`

Submits the form related to the input element using `Rails.fire()`. This is for use on `change` events on input elements.

##### Example

```erb
<%= form_with model: @model, data: { controller: 'remote', action: 'ajax:error->remote#replace' } do |f| %>
  <%= f.text_field :field_name, data: { action: 'change->remote#submit' } %>
  <%= f.submit %>
<% end %>
```

#### `debounceSubmit`

The same as `submit` but debounced. This allows you to submit a form when the user has finished typing in a field.

```erb
<%= form_with model: @model, data: { controller: 'remote', action: 'ajax:error->remote#replace' } do |f| %>
  <%= f.text_field :field_name, data: { action: 'input->remote#debounceSubmit' } %>
  <%= f.submit %>
<% end %>
```

#### `replace`

The replace method is used to replace the response target.

##### Example

```haml
= form_with model: @model, data: { controller: 'remote', action: 'ajax:error->remote#error' } do |f|
  = f.text_field :field_name
  = f.submit
```

#### `replaceInner`

The replaceInner method is used to replace the contents of the response target (`innerHTML`).

##### Example

```erb
<div data-controller="remote">
  <%= link_to 'Click me to replace me', foo_path, data: { action: 'ajax:success->remote#replaceInner' } %>
</div>
```

#### `append`

The append method is used to append the content of the response to the response target.

##### Example

```erb
<div data-controller="remote">
  <%= link_to 'Click me to append my response', foo_path, data: { action: 'ajax:success->remote#append' } %>
  <div>Some content</div>
  <!-- will append response here -->
</div>
```

#### `prepend`

The replace method is used to prepend the content of the response to the response target.

##### Example

```erb
<div data-controller="remote">
  <!-- will append response here -->
  <div>Some content</div>
  <%= link_to 'Click me to prepend my response', foo_path, data: { action: 'ajax:success->remote#append' } %>
</div>
```

#### `remove`

The remove method is used to remove the response target.

##### Example

```erb
<div data-controller="remote">
  <!-- will append response here -->
  <div>Some content</div>
  <%= link_to 'Click me to remove me', foo_path, data: { action: 'ajax:success->remote#remove' } %>
</div>
```


## Contributing

Fork the project.

Install dependencies
```shell
$ yarn install
```

Start the test watcher
```shell
$ yarn test:watch
```

Running one-off test runs can be done with:
```shell
$ yarn test
```

Write some tests, and add your feature. Send a PR.
